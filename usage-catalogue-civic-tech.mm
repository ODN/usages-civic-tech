<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Usages du catalogue Civic Tech" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1464431166346"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="4"/>
<node TEXT="Objectifs" POSITION="right" ID="ID_1449400195" CREATED="1461172360857" MODIFIED="1461175354751">
<edge COLOR="#0000ff"/>
<node TEXT="cr&#xe9;er un catalogue des Civic Tech en s&apos;appuyant sur tous les catalogues existants" ID="ID_1718308381" CREATED="1461172374940" MODIFIED="1461172398711">
<node TEXT="Listing n&#xe9;cessaire :" ID="ID_526462624" CREATED="1461172560361" MODIFIED="1461172655966">
<node TEXT="&#x2192; adresser ces fonctionnalit&#xe9;s pendant le hackathon (cf toutes les fonctionnalit&#xe9;s civic tech)" ID="ID_1786904219" CREATED="1461172658652" MODIFIED="1461172660139">
<node TEXT="ne pas limiter la liste a priori (chacun &#xe9;tant libre de labelliser selon ses crit&#xe8;res)" ID="ID_791263631" CREATED="1461172774234" MODIFIED="1461172801909"/>
</node>
<node TEXT="- lister les cat&#xe9;gories d&#x2019;utilisateurs" ID="ID_23292937" CREATED="1461172661169" MODIFIED="1461172666750">
<node TEXT="secteur public" ID="ID_59260579" CREATED="1461173356424" MODIFIED="1461173359489"/>
<node TEXT="collectifs" ID="ID_1022278961" CREATED="1461173359936" MODIFIED="1461173371475"/>
</node>
</node>
<node TEXT="Travailler sur les fonctionnalit&#xe9;s de chaque appli (mobiliser des publics, analyser ce qui a &#xe9;t&#xe9; collecter en aval, etc.)" ID="ID_1497743895" CREATED="1461172612197" MODIFIED="1461172613567"/>
<node TEXT="ajouter les dimensions m&#xe9;thodologies / &#xe9;thiques" ID="ID_1423666319" CREATED="1461172577167" MODIFIED="1461172594849">
<node TEXT="transparence" ID="ID_1707533257" CREATED="1461173416263" MODIFIED="1461173422183">
<node TEXT="par ex. autorisation si APIs, etc." ID="ID_1363179635" CREATED="1461173437399" MODIFIED="1461173457296"/>
<node TEXT="concertation / participation" ID="ID_1347776412" CREATED="1461174118137" MODIFIED="1461174128015"/>
</node>
<node TEXT="principe du &quot;rendre compte&quot;" ID="ID_1806875614" CREATED="1461174128979" MODIFIED="1461174138164"/>
</node>
<node TEXT="penser au continuum consertationss physiques / internet" ID="ID_949366945" CREATED="1461173715687" MODIFIED="1461173787093"/>
<node TEXT="travailler sur les standards / normes" ID="ID_1161900954" CREATED="1461172622906" MODIFIED="1461172632596"/>
<node TEXT="indicateurs sur la qualit&#xe9; : impliquer la ct&#xe9; de chercheurs / scientifique / sociologie du langage" ID="ID_801384243" CREATED="1461172924923" MODIFIED="1461172943738"/>
</node>
<node TEXT="logiciels / exstensions qui permettent d&#x2019;adresser des besoins." ID="ID_537606959" CREATED="1461172408218" MODIFIED="1461175354747" VSHIFT="-40"/>
</node>
<node TEXT="Usages" POSITION="right" ID="ID_1152151753" CREATED="1461172335004" MODIFIED="1461175234807">
<edge COLOR="#ff0000"/>
<node TEXT="Administration" ID="ID_1579266803" CREATED="1461172342760" MODIFIED="1461172346214">
<node TEXT="identifier les pistes de mutualisation pour cofinancer des d&#xe9;veloppements" ID="ID_1953982010" CREATED="1461172427451" MODIFIED="1461172441629"/>
<node TEXT="partager des types des sc&#xe9;narios / combinaisons de logiciels" ID="ID_1531326149" CREATED="1461174055827" MODIFIED="1461175317274"/>
</node>
<node TEXT="Citoyens" ID="ID_331031207" CREATED="1461172346600" MODIFIED="1461172948902">
<node TEXT="apporter de la confiance grace &#xe0; une meilleure compr&#xe9;hension des outils" ID="ID_825213402" CREATED="1461172983882" MODIFIED="1461175303843"/>
<node TEXT="pousser le format aupr&#xe8;s des &#xe9;lus" ID="ID_1852724432" CREATED="1461175151660" MODIFIED="1461175226899"/>
</node>
</node>
<node TEXT="Services" POSITION="right" ID="ID_826915862" CREATED="1461172446532" MODIFIED="1461172672030">
<edge COLOR="#00ff00"/>
<node TEXT=" sous plateforme permettant de guider le choix" ID="ID_212561621" CREATED="1461172470933" MODIFIED="1461172488989">
<node TEXT="quelles fonctionnalit&#xe9;s" ID="ID_1410750136" CREATED="1461172516081" MODIFIED="1461172523859"/>
<node TEXT="au travers d&apos;une ergonomie accompagnant le choix (pour &#xe9;viter les biais)" ID="ID_1447911092" CREATED="1461173802779" MODIFIED="1461173864610"/>
<node TEXT="quels crit&#xe8;res" ID="ID_1519631345" CREATED="1461172524430" MODIFIED="1461172529168"/>
</node>
<node TEXT="couche ludique pour favoriser le crowdsourcing" ID="ID_1275836763" CREATED="1461172534575" MODIFIED="1461172672025">
<node TEXT="les r&#xe8;gles doivent &#xea;tre claires et coconstruites" ID="ID_846078116" CREATED="1461173911345" MODIFIED="1461175337581"/>
</node>
<node TEXT="n&#xe9;cessit&#xe9; de pouvoir exporter les donn&#xe9;es" ID="ID_1147734380" CREATED="1461172678731" MODIFIED="1461172687615"/>
</node>
<node TEXT="autres informations connexes" POSITION="right" ID="ID_665844936" CREATED="1461172701438" MODIFIED="1461172709836">
<edge COLOR="#ff00ff"/>
<node TEXT="lister les fournisseurs" ID="ID_661406989" CREATED="1461172711838" MODIFIED="1461172722149">
<node TEXT="Charte Chat.on : d&#xe9;finir le respects de certains crit&#xe8;res" ID="ID_836997119" CREATED="1461172722490" MODIFIED="1461172745196"/>
</node>
<node TEXT="Besoin de r&#xe9;unir la communaut&#xe9; pour partager ses r&#xe9;flexions" ID="ID_1448566449" CREATED="1461172820241" MODIFIED="1461172911259"/>
<node TEXT="Guide &#xe0; destination des administrations/coll publiques (peu &#xe9;quip&#xe9;e" ID="ID_96176354" CREATED="1461172489453" MODIFIED="1461172840067">
<node TEXT="pour op&#xe9;rer une consultation" ID="ID_1538025575" CREATED="1461172860269" MODIFIED="1461172864935"/>
<node TEXT="pour entrer dans une d&#xe9;marche d&apos;Open Gov" ID="ID_1088384197" CREATED="1461172865172" MODIFIED="1461172898555"/>
</node>
<node TEXT="partage des bonnes pratiques" ID="ID_1562855677" CREATED="1461174940094" MODIFIED="1461174949794"/>
<node TEXT="education populaire ?" ID="ID_1660593183" CREATED="1461175052908" MODIFIED="1461175067127"/>
</node>
</node>
</map>